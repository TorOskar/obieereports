package reportsfetcher;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Iterator;
import java.util.Map;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

import oracle.tip.mediator.common.api.AbstractJavaCalloutImpl;
import oracle.tip.mediator.common.api.CalloutMediatorMessage;
import oracle.tip.mediator.common.api.IJavaCallout;
import oracle.tip.mediator.common.api.MediatorCalloutException;
import oracle.tip.mediator.utils.XmlUtils;

import oracle.xml.parser.v2.XMLDocument; 
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;

import org.w3c.dom.Node;


public class ReportExclusion extends AbstractJavaCalloutImpl implements IJavaCallout {
    public ReportExclusion() {
    }

    @Override
    public boolean preRouting(CalloutMediatorMessage message) throws MediatorCalloutException {
        System.out.println("Start java callout..");

        String sPayload = null;
        String sPayload_org = null;
        System.out.println("Message");
        System.out.println(message.getPayload());
        for (Iterator msgIt = message.getPayload().entrySet().iterator(); msgIt.hasNext();) {
            Map.Entry msgEntry = (Map.Entry) msgIt.next();
            Object msgKey = msgEntry.getKey();
            Object msgValue = msgEntry.getValue();

            if (msgKey.equals("part1"))
                sPayload = XmlUtils.convertDomNodeToString((Node) msgValue);

        }
        System.out.println("Payload");
        sPayload_org = sPayload;
        System.out.println(sPayload);

        XMLDocument changedoc;
        try {
            String queryExpressionClient =
                "declare namespace rep='http://messages.gard.no/v1_0/ReportParameters';" +
                "$this/rep:ReportParameters/rep:ClientId";
            String queryExpressionParameterString =
                "declare namespace rep='http://messages.gard.no/v1_0/ReportParameters';" +
                "$this/rep:ReportParameters/rep:ParameterString";
            String queryExpressionIsBroker =
                "declare namespace rep='http://messages.gard.no/v1_0/ReportParameters';" +
                "$this/rep:ReportParameters/rep:IsBroker";
            
            XmlObject objectToInsert = XmlObject.Factory.parse(sPayload);
            System.out.println("ObjectToInser");
            System.out.println(objectToInsert.xmlText());
            XmlObject[] xobs = objectToInsert.selectPath(queryExpressionIsBroker);
            String isBroker = null;
            for (XmlObject xob : xobs) {
                if (xobs.length > 1) {
                    throw new Exception("Cannot find isBroker");
                }
                isBroker = xob.newCursor().getTextValue();
            }
            
            xobs = objectToInsert.selectPath(queryExpressionClient);
            String client = null;
            for (XmlObject xob : xobs) {
                if (xobs.length > 1) {
                    throw new Exception("Cannot find client");
                }
                client = xob.newCursor().getTextValue();
            }
            xobs = objectToInsert.selectPath(queryExpressionParameterString);
            String broker = null;
            String parameterstring ="";
            String[] parameters = null;
            for (XmlObject xob : xobs) {
                if (xobs.length > 1) {
                    throw new Exception("Cannot find broker");
                }
                parameterstring = xob.newCursor().getTextValue();
                parameters = parameterstring.split(",");
                broker = parameters[0];

            }

            if (!parameterstring.isEmpty())
            {
                if (!isBrokerExcluded(client, broker)) {
                    parameterstring = parameterstring.replace(parameters[0], "");
                    isBroker = "false";
                    
                    
                }
                else {
                    isBroker = "true";
                    
                }
            }           
            
            
            xobs = objectToInsert.selectPath(queryExpressionIsBroker);
            for (XmlObject xob : xobs) {
                if (xobs.length > 1) {
                        throw new Exception("Multiple isBroker");
                }
                xob.newCursor().setTextValue(String.valueOf(isBroker));
            }
            xobs = objectToInsert.selectPath(queryExpressionParameterString);
            for (XmlObject xob : xobs) {
                if (xobs.length > 1) {
                        throw new Exception("Multiple parameterstrings");
                }
                xob.newCursor().setTextValue(String.valueOf(parameterstring));
            }
            System.out.println("ObjectToInser finished");
            System.out.println(objectToInsert.xmlText());


            //throw new MediatorCalloutException(xob.xmlText());
            changedoc = XmlUtils.getXmlDocument(objectToInsert.xmlText());
            String mykey = "part1";
            message.addPayload(mykey, changedoc.getDocumentElement());
            //calloutMediatorMessage.getPayload().put(mykey, changedoc);
        } catch (XmlException ex) {
            System.out.println("XML FEIL");
            ex.printStackTrace();
            throw new MediatorCalloutException(ex.getMessage());
        } catch (Exception ex) {
            //System.out.println("Getdocument");
            ex.printStackTrace();
            //throw new MediatorCalloutException(ex.getMessage());
        }

        return false;

    }

   

    Connection getJNDIConnection() throws Exception {
        String DATASOURCE_CONTEXT = "java:jdbc/GARD_MDM";

        Connection result = null;
        try {
            Context initialContext = new InitialContext();

            DataSource datasource = (DataSource) initialContext.lookup(DATASOURCE_CONTEXT);
            if (datasource != null) {
                result = datasource.getConnection();
            } else {

            }
        } catch (NamingException ex) {
            throw new Exception("Cannot find JNDI resource", ex);

        } catch (SQLException ex) {
            throw new Exception("Cannot connect to db", ex);
        }
        return result;
    }

    public boolean isBrokerExcluded(String clientId, String brokerId) throws Exception {
        Connection sqlConn = null;
        Statement statement = null;

        String sql = "SELECT * " + "FROM REPORTEXCLUSIONS " + "WHERE CLIENTID = " + clientId +" AND BROKERID= "+ brokerId;
        
        try {
            sqlConn = getConnection();
            statement = sqlConn.createStatement();
            //checkDBConnection();
            ResultSet rs = statement.executeQuery(sql);
            if (!rs.next()) {
                return false;
            }
            return true;
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new Exception(se);
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
            throw new Exception(e);

        } finally {
            //finally block used to close resources
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException se) {
            } // do nothing
            try {
                if (sqlConn != null)
                    sqlConn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } //end finally try

        }


    }
    public synchronized Connection getConnection() throws Exception{
                    Connection connection = null;
                                    
                    try
                    {
                            
                            
                            Class.forName("oracle.jdbc.driver.OracleDriver");
                            //connection = DriverManager.getConnection("jdbc:oracle:thin:@//areoda02-scan.gard.local:1521/mdmtst01.gard.local", "GARD_MDM","dwh4all");
                            connection = DriverManager.getConnection("jdbc:oracle:thin:@//areoda01-scan.gard.local:1521/mdmprd.gard.local", "GARD_MDM","dwh4all");
                            //connection = DriverManager.getConnection("jdbc:oracle:thin:@//areoda02-scan.gard.local:1521/mdmdev01.gard.local", "MDM_DEV","hopp50");
                            
                    } catch (Exception ce) {
                            
                            throw new Exception("Kan ikke koble til databasen",ce);
                            //ce.printStackTrace();
                    }
                    /*Context ctx = null;
                      Hashtable ht = new Hashtable();
                      ht.put(Context.INITIAL_CONTEXT_FACTORY,"weblogic.jndi.WLInitialContextFactory");
                      //ht.put(Context.PROVIDER_URL,"t3://aresrv20.gard.local:7032");
                      //ht.put(Context.PROVIDER_URL,"t3://localhost:7001");
                      ht.put(Context.PROVIDER_URL,"t3://192.168.24.19:7001"); 
                     
                       try {
                        ctx = new InitialContext(ht);
                        javax.sql.DataSource ds 
                          = (javax.sql.DataSource) ctx.lookup ("jdbc.DB.MDM");
                        connection = ds.getConnection();
                       }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                        }*/
                    return connection;

            }
}
