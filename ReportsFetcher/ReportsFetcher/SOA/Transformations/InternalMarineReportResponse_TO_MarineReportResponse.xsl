<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java"
                xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
                xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
                xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
                xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
                xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas"
                xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
                xmlns:ns0="http://messages.gard.no/v1_0/ReportResponse" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xsd xsi oracle-xsl-mapper xsl ns0 oraxsl xp20 xref mhdr oraext dvm socket"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
                xmlns:inp1="http://messages.gard.no/v1_0/MarineReportParameters"
                xmlns:tns="http://xmlns.oracle.com/ReportsApplication/ReportsFetcher/MarineMediator"
                xmlns:ns1="http://wsdl.gard.no/v1_0/ReportsService"
                xmlns:ns2="http://messages.gard.no/v1_0/ReportParameters">
    <oracle-xsl-mapper:schema>
        <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
        <oracle-xsl-mapper:mapSources>
            <oracle-xsl-mapper:source type="WSDL">
                <oracle-xsl-mapper:schema location="../WSDLs/MarineMediator.wsdl"/>
                <oracle-xsl-mapper:rootElement name="ReportResponse"
                                               namespace="http://messages.gard.no/v1_0/ReportResponse"/>
            </oracle-xsl-mapper:source>
        </oracle-xsl-mapper:mapSources>
        <oracle-xsl-mapper:mapTargets>
            <oracle-xsl-mapper:target type="WSDL">
                <oracle-xsl-mapper:schema location="oramds:/apps/WSDL/V1/Reports/ReportsService.wsdl"/>
                <oracle-xsl-mapper:rootElement name="ReportResponse"
                                               namespace="http://messages.gard.no/v1_0/ReportResponse"/>
            </oracle-xsl-mapper:target>
        </oracle-xsl-mapper:mapTargets>
        <!--GENERATED BY ORACLE XSL MAPPER 12.1.3.0.0(XSLT Build 140529.0700.0211) AT [THU JUL 30 20:06:58 CEST 2015].-->
    </oracle-xsl-mapper:schema>
    <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
    <xsl:template match="/">
        <ns0:ReportResponse>
            <ns0:Report>
                <xsl:value-of select="/ns0:ReportResponse/ns0:Report"/>
            </ns0:Report>
        </ns0:ReportResponse>
    </xsl:template>
</xsl:stylesheet>
