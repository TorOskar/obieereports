package no.gard.reports;

import javax.ejb.Remote;

@Remote
public interface ObieeAllReportsEjb {
    public String fetchReport(String dashboard, String reportName, String parameters, String isBroker, String userAgent);
    public String getMarineReport(String username, String password,int clientId, String dashboard, String reportName, String parameters,String asatDate, boolean isBroker,String userAgent);
    public String getReport(String username, String password,int clientId, String dashboard, String reportName, String parameters,String asatDate, boolean isBroker, boolean showPeopleClaim,String userAgent);
    
    
}
