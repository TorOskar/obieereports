package no.gard.reports;

import javax.annotation.Resource;

import javax.ejb.Local;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import no.gard.obiee.OBIEEClient;

import org.apache.commons.codec.binary.Base64;

@Stateless(name = "ObieeAllReportsEjb", mappedName = "ObieeAllReportsEjb")
@Local
public class ObieeAllReportsEjbBean implements ObieeAllReportsEjb {
    @Resource
    SessionContext sessionContext;
    private static String ENDPOINT = "http://aresrv641.gard.local:9704";
    private static String PATH = "/mobanalytics/saw.dll?Dashboard";


    public ObieeAllReportsEjbBean() {
    }
    public String fetchReport(String dashboard, String reportName, String parameters, String isBroker, String userAgent) {
          
        String result = OBIEEClient.getReportStatically("","BIEXTRANETUSER","Jg5#Om@K4(G", "/shared/Extranet/_portal/Renewal Reports", "Vessels On Risk", "Print","2016-02-20",true,"4","eq","\"Dim Client\".\"Sf Company Id\"","1710","eq","\"Dim Coverage\".\"Gic Coverage Group Code\"","PI", "eq", "\"Dim Agreement Type\".\"Agreement Type Code\"","OWN","eq","\"Dim Broker\".\"Sf Company Id\"","35979");
        
        byte[]   bytesEncoded = Base64.encodeBase64(result.getBytes());
        return  new String(bytesEncoded );
         //return "kvakk";  
    }
    public String getReport(String username, String password,int clientId,String dashboard,String reportName, String parameters, String asatDate,boolean isBroker, boolean showPeopleClaim,String userAgent) {
          
        String result = "";
        String broker = "";
        String covertype="PI";
        String agreementType = "OWN";
        String[] parametersArray =  parameters.split(",");
        if (parametersArray.length == 1) {
             broker = parametersArray[0];
        }
        else if (parametersArray.length == 2) {
            if (parametersArray[0] !="")
            {
                broker = parametersArray[0];
            }
            if (parametersArray[1] !="")
            {
                covertype = parametersArray[1];
            }
        }
        else if (parametersArray.length == 3) {
            if (parametersArray[0] !="")
            {
                broker = parametersArray[0];
            }
            if (parametersArray[1] !="")
            {
                covertype = parametersArray[1];
            }
            if (parametersArray[2] !="")
            {
                agreementType  = parametersArray[2]; 
            }
        }
        
        
        
        
        if (isBroker)
        {      
            result = OBIEEClient.getReportStatically(ENDPOINT+PATH,username,password, dashboard,   reportName,"Print",asatDate,showPeopleClaim,"4","eq","\"Dim Client\".\"Sf Company Id\"",String.valueOf(clientId),"eq","\"Dim Coverage\".\"Gic Coverage Group Code\"",covertype, "eq", "\"Dim Agreement Type\".\"Agreement Type Code\"",agreementType,"eq","\"Dim Broker\".\"Sf Company Id\"",broker);
        }
        else {
            result = OBIEEClient.getReportStatically(ENDPOINT+PATH,username,password, dashboard,   reportName,"Print",asatDate,showPeopleClaim,"3","eq","\"Dim Client\".\"Sf Company Id\"",String.valueOf(clientId),"eq","\"Dim Coverage\".\"Gic Coverage Group Code\"",covertype, "eq", "\"Dim Agreement Type\".\"Agreement Type Code\"",agreementType); 
        }
        byte[]   bytesEncoded = Base64.encodeBase64(result.getBytes());
        return  new String(bytesEncoded );
    }
    public String getMarineReport(String username, String password,int clientId,String dashboard,String reportName, String parameters,String asatDate, boolean isBroker,String userAgent) {
          
        String result = "";
        String broker = "";
        String covertype="HU";
       
        String[] parametersArray =  parameters.split(",");
        if (parametersArray.length == 1) {
             broker = parametersArray[0];
        }
        else if (parametersArray.length == 2) {
            if (parametersArray[0] !="")
            {
                broker = parametersArray[0];
            }
            if (parametersArray[1] !="")
            {
                covertype = parametersArray[1];
            }
        }
        
        
        
        
        System.out.println("Asat date: "+asatDate);
        if (isBroker)
        {      
            //result = OBIEEClient.getReportStatically(ENDPOINT+PATH,username,password, dashboard,   reportName,"Print",asatDate,false,"3","eq","\"Dim Client\".\"Sf Company Id\"",String.valueOf(clientId),"eq","\"Dim Coverage\".\"Gic Coverage Group Code\"",covertype, "eq","\"Dim Broker\".\"Sf Company Id\"",broker);
            
            OBIEEClient oc = new OBIEEClient();
            
            result = oc.getReportStatefully(ENDPOINT+PATH,username,password, dashboard,   reportName,"Print",asatDate,false,"3","eq","\"Dim Client\".\"Sf Company Id\"",String.valueOf(clientId),"eq","\"Dim Coverage\".\"Gic Coverage Group Code\"",covertype, "eq","\"Dim Broker\".\"Sf Company Id\"",broker);
        }
        else {
            OBIEEClient oc = new OBIEEClient();
            
            result = oc.getReportStatefully(ENDPOINT+PATH,username,password, dashboard,   reportName,"Print",asatDate,false,"2","eq","\"Dim Client\".\"Sf Company Id\"",String.valueOf(clientId),"eq","\"Dim Coverage\".\"Gic Coverage Group Code\"",covertype); 
            //result = OBIEEClient.getReportStatically(ENDPOINT+PATH,username,password, dashboard,   reportName,"Print",asatDate,false,"2","eq","\"Dim Client\".\"Sf Company Id\"",String.valueOf(clientId),"eq","\"Dim Coverage\".\"Gic Coverage Group Code\"",covertype); 
        }
        byte[]   bytesEncoded = Base64.encodeBase64(result.getBytes());
        return  new String(bytesEncoded );
    }
}
