package no.gard.obiee;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.TextPage;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OBIEEClient {

    private static Logger LOG = Logger.getLogger(OBIEEClient.class);

    private static Pattern PATTERN_LINK = Pattern.compile("<link (.+?)>");

    private static Pattern PATTERN_CSS = Pattern.compile("(?i)rel=\"Stylesheet\"");

    private static Pattern PATTERN_HREF = Pattern.compile("href=\"(.+?)\"");

    // This pattern will validate date format YYYY-MM-dd between Feb. 20th 2014 and Dec. 31st 2399.
    // It enforces 28/30/31 days in each month and will accept Feb 29th in leap years only.
    private static Pattern PATTERN_AS_AT = Pattern.compile(
            // 2014
            "^(2014-02-2[0-8]|2014-((0[469]|11)-(0[1-9]|[12][0-9]|30)|(0[3578]|1[02])-(0[0-9]|[12][0-9]|3[01]))|" +
            // 2015 - 2399
            "((20(1[5-9]|[2-9][0-9])|2[1-3][0-9]{2})-((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01])|(0[469]|11)-(0[1-9]|[12][0-9]|30)|02-(0[1-9]|1[0-9]|2[0-8])))|" +
            // Leap year correction
            "(2016|20([2468][048]|[3579][26])|2[1-3]([02468][048]|[13579][26]))-02-29)$");


    private WebClient webClient;


    public static String getReportStatically(String urlAsString, String username, String password, String portalPath, String page, String action, String renewalReportsAsAt, boolean allowPeopleClaim, String... params) throws OBIEEClientException {

        String parameters = buildParameterString(username, password, portalPath, page, action, renewalReportsAsAt, allowPeopleClaim, params);
        System.out.println(parameters);
        return getReportStatically(urlAsString, parameters);
    }


    public static String getReportStatically(final String urlAsString, final String parameters) throws OBIEEClientException {

        WebClient webClient = new WebClient();
        return getReport(webClient, urlAsString, parameters);
    }


    private static String expandLinkedCSS(final HtmlPage htmlPage) throws IOException {

        String responseBody = htmlPage.getWebResponse().getContentAsString();
        Matcher linkMatcher = PATTERN_LINK.matcher(responseBody);

        while (linkMatcher.find()) {
            String linkElement = linkMatcher.group();
            Matcher stylesheetMatcher = PATTERN_CSS.matcher(linkElement);
            if (stylesheetMatcher.find()) {
                LOG.debug(String.format("Found CSS link [%s]", linkElement));
                Matcher hrefMatcher = PATTERN_HREF.matcher(linkElement);

                if (hrefMatcher.find()) {
                    String href = hrefMatcher.group().replace("href=\"", "").replace("\"", "");
                    URL url = htmlPage.getFullyQualifiedUrl(href);
                    LOG.debug(String.format("Fetching [%s]", url.toExternalForm()));
                    WebRequest getRequest = new WebRequest(url, HttpMethod.GET);
                    TextPage cssPage = htmlPage.getWebClient().getPage(getRequest);
                    String css = cssPage.getWebResponse().getContentAsString();
                    LOG.debug(String.format("Fetched [%s]", css));
                    responseBody = responseBody.replace(linkElement, "<style>" + css + "</style>");
                }
            }
        }

        return responseBody;
    }

    private static String buildParameterString(String username, String password, String portalPath, String page, String action, String renewalReportAsAt, boolean allowPeopleClaim, String... params) {

        StringBuilder builder = new StringBuilder();
        builder.append("NQuser=").append(username).append("&");
        builder.append("NQpassword=").append(password).append("&");
        builder.append("PortalPath=").append(portalPath).append("&");
        builder.append("Page=").append(page).append("&");
        builder.append("Action=").append(action).append("&");

        if (renewalReportAsAt != null) {
            if (PATTERN_AS_AT.matcher(renewalReportAsAt).matches()) {
                builder.append("RenewalReportsAsAt=").append(renewalReportAsAt).append("&");
            } else {
                LOG.error("Invalid value for RenewalReportsAsAt. Will omit in request, defaulting to current date.");
                System.out.println("Invalid value for RenewalReportsAsAt. Will omit in request, defaulting to current date.");
            }
        } else {
        	System.out.println("RenewalReportsAsAt omitted");
            LOG.debug("RenewalReportsAsAt omitted");
        }

        int paramIndex = 0;
        for (String p : params) {
            builder.append("P").append(paramIndex++).append("=").append(p).append("&");
        }

        builder.append("AllowPeopleClaim=").append(allowPeopleClaim ? "Y" : "N");

        return builder.toString();
    }


    private static String getReport(final WebClient webClient, final String urlAsString, final String parameters) {

        try {
            WebRequest request = new WebRequest(new URL(urlAsString), HttpMethod.POST);
            request.setRequestBody(parameters);
            System.out.println(request.getRequestBody());
            HtmlPage htmlPage = webClient.getPage(request);
            return expandLinkedCSS(htmlPage);
        } catch (Exception e) {
            throw new OBIEEClientException(e);
        }
    }


    public String getReportStatefully(final String urlAsString, final String parameters) throws OBIEEClientException {

        initialize();
        return getReport(webClient, urlAsString, parameters);
    }


    public String getReportStatefully(String urlAsString, String username, String password, String portalPath, String page, String action, String renewalReportsAsAt, boolean allowPeopleClaim, String... params) throws OBIEEClientException {

        String parameters = buildParameterString(username, password, portalPath, page, action, renewalReportsAsAt, allowPeopleClaim, params);
        return getReportStatefully(urlAsString, parameters);
    }


    private synchronized void initialize() {

        if (webClient == null) {
            webClient = new WebClient();
        }
    }
}
