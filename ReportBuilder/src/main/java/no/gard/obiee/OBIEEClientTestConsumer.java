package no.gard.obiee;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class OBIEEClientTestConsumer {

    private static String ENDPOINT = "http://aresrv641.gard.local:9704";

    private static String PATH = "/mobanalytics/saw.dll?Dashboard";

    public static void main(String[] args) {

//        for (int i = 0; i < 10; i++) {
//            long start = System.currentTimeMillis();
//            OBIEEClient.getReportStatically(
//                    ENDPOINT + PATH,
//                    "weblogic",
//                    "biGard1911",
//                    "/shared/Extranet/_portal/Renewal Reports",
//                    "Vessels On Risk",
//                    "Print",
//                    true,
//                    "4",
//                    "eq",
//                    "\"Dim Client\".\"Sf Company Id\"",
//                    "1710",
//                    "eq",
//                    "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                    "PI",
//                    "eq",
//                    "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                    "OWN",
//                    "eq",
//                    "\"Dim Broker\".\"Sf Company Id\"",
//                    "35979");
//            long elapsed = System.currentTimeMillis() - start;
//            System.out.println(String.format("Static run %d: %d ms", i + 1, elapsed));
//        }
//
//
//        OBIEEClient client = new OBIEEClient();
//        for (int i = 0; i < 10; i++) {
//            long start = System.currentTimeMillis();
//            client.getReportStatefully(
//                    ENDPOINT + PATH,
//                    "weblogic",
//                    "biGard1911",
//                    "/shared/Extranet/_portal/Renewal Reports",
//                    "Vessels On Risk",
//                    "Print",
//                    true,
//                    "4",
//                    "eq",
//                    "\"Dim Client\".\"Sf Company Id\"",
//                    "1710",
//                    "eq",
//                    "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                    "PI",
//                    "eq",
//                    "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                    "OWN",
//                    "eq",
//                    "\"Dim Broker\".\"Sf Company Id\"",
//                    "35979");
//
//            client.getReportStatefully(
//                ENDPOINT + PATH,
//                "weblogic",
//                "biGard1911",
//                "/shared/Extranet/_portal/Renewal Reports",
//                "Vessels On Risk",
//                "Print",
//                true,
//                "3",
//                "eq",
//                "\"Dim Client\".\"Sf Company Id\"",
//                "4472",
//                "eq",
//                "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                "PI",
//                "eq",
//                "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                "OWN");
//
//            long elapsed = System.currentTimeMillis() - start;
//            System.out.println(String.format("Stateful run %d: %d ms", i + 1, elapsed));
//        }

//        long start1 = System.currentTimeMillis();
//


        /*
        * "Loss Ratios"
        * "Distribution of RI Costs etc. Per Year"
        * "Vessels On Risk"
        * "Claims Analysis"
        * "Claims In Excess of Abatement"
        * "All Claims"
        * */

            String result = OBIEEClient.getReportStatically(
                    ENDPOINT + PATH,
                    "BIEXTRANETUSER",
                    "Jg5#Om@K4(G",
                    "/shared/Extranet/_portal/Renewal Reports",
                    "All Claims",
                    "Print",
                    "2014-05-13",
                    true, //Allow people claims
                    "4",
                    "eq",
                    "\"Dim Client\".\"Sf Company Id\"",
                    "1710",
                    "eq",
                    "\"Dim Coverage\".\"Gic Coverage Group Code\"",
                    "PI",
                    "eq",
                    "\"Dim Agreement Type\".\"Agreement Type Code\"",
                    "OWN",
                    "eq",
                    "\"Dim Broker\".\"Sf Company Id\"",
                    "35979");

        System.out.println(result);
//
//        long lap1 = System.currentTimeMillis() - start1;
//
//            OBIEEClient.getReportStatically(
//                    ENDPOINT + PATH,
//                    "weblogic",
//                    "biGard1911",
//                    "/shared/Extranet/_portal/Renewal Reports",
//                    "Vessels On Risk",
//                    "Print",
//                    true,
//                    "3",
//                    "eq",
//                    "\"Dim Client\".\"Sf Company Id\"",
//                    "4472",
//                    "eq",
//                    "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                    "PI",
//                    "eq",
//                    "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                    "OWN");
//
//        long lap2 = System.currentTimeMillis() - start1 - lap1;
//
//        System.out.println(String.format("Static fetch: Lap 1: %d, Lap2: %d", lap1, lap2));
//
//        OBIEEClient client = new OBIEEClient();
//
//        long start2 = System.currentTimeMillis();
//
//        client.getReportStatefully(
//                ENDPOINT + PATH,
//                "weblogic",
//                "biGard1911",
//                "/shared/Extranet/_portal/Renewal Reports",
//                "Vessels On Risk",
//                "Print",
//                true,
//                "4",
//                "eq",
//                "\"Dim Client\".\"Sf Company Id\"",
//                "1710",
//                "eq",
//                "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                "PI",
//                "eq",
//                "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                "OWN",
//                "eq",
//                "\"Dim Broker\".\"Sf Company Id\"",
//                "35979");
//
//        long lap21 = System.currentTimeMillis() - start2;
//
//        client.getReportStatefully(
//                ENDPOINT + PATH,
//                "weblogic",
//                "biGard1911",
//                "/shared/Extranet/_portal/Renewal Reports",
//                "Vessels On Risk",
//                "Print",
//                true,
//                "3",
//                "eq",
//                "\"Dim Client\".\"Sf Company Id\"",
//                "4472",
//                "eq",
//                "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                "PI",
//                "eq",
//                "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                "OWN");
//
//        long lap22 = System.currentTimeMillis() - start2 - lap21;
//
//        System.out.println(String.format("Stateful fetch: Lap 1: %d, Lap2: %d", lap21, lap22));
    }
}
