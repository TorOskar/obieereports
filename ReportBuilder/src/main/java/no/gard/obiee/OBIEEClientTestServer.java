package no.gard.obiee;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class OBIEEClientTestServer {

    private static String ENDPOINT = "http://aresrv641.gard.local:9704";

    private static String PATH = "/mobanalytics/saw.dll?Dashboard";

    public static void main(String[] args) {

        try {
            ServerSocket socket = new ServerSocket(9191);
            Socket clientSocket = socket.accept();

            BufferedWriter out = new BufferedWriter(new PrintWriter(clientSocket.getOutputStream(), true), 2097152);

            String body = OBIEEClient.getReportStatically(
                    ENDPOINT + PATH,
                    "weblogic",
                    "biGard1911",
                    "/shared/Extranet/_portal/Renewal Reports",
                    "Vessels On Risk",
                    "Print",
                    "",
                    true,
                    "4",
                    "eq",
                    "\"Dim Client\".\"Sf Company Id\"",
                    "1710",
                    "eq",
                    "\"Dim Coverage\".\"Gic Coverage Group Code\"",
                    "PI",
                    "eq",
                    "\"Dim Agreement Type\".\"Agreement Type Code\"",
                    "OWN",
                    "eq",
                    "\"Dim Broker\".\"Sf Company Id\"",
                    "35979");

            out.write(body);
            out.flush();
            clientSocket.close();
            socket.close();

            socket = new ServerSocket(9191);
            clientSocket = socket.accept();

            out = new BufferedWriter(new PrintWriter(clientSocket.getOutputStream(), true), 2097152);
            body = OBIEEClient.getReportStatically(
                    ENDPOINT + PATH,
                    "weblogic",
                    "biGard1911",
                    "/shared/Extranet/_portal/Renewal Reports",
                    "Vessels On Risk",
                    "Print",
                    "",
                    true,
                    "3",
                    "eq",
                    "\"Dim Client\".\"Sf Company Id\"",
                    "4472",
                    "eq",
                    "\"Dim Coverage\".\"Gic Coverage Group Code\"",
                    "PI",
                    "eq",
                    "\"Dim Agreement Type\".\"Agreement Type Code\"",
                    "OWN");


            out.write(body);
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
