package no.gard.obiee;

public class OBIEEClientException extends RuntimeException {

    public OBIEEClientException() {
    }

    public OBIEEClientException(String message) {
        super(message);
    }

    public OBIEEClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public OBIEEClientException(Throwable cause) {
        super(cause);
    }

    public OBIEEClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
