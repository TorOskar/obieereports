package no.gard.obiee;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runners.JUnit4;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nilnil on 30/03/2015.
 */
public class OBIEEClientTest {

    @Test
    public void testGetReport() {



//        Pattern ex = Pattern.compile(
//                "(((19|20)([2468][048]|[13579][26]|0[48])|2000)[/-]02[/-]29|" + //Leap years
//                "((19|20)[0-9]{2}[/-](0[4678]|1[02])[/-](0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}[/-](0[1359]|11)[/-](0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}[/-]02[/-](0[1-9]|1[0-9]|2[0-8])))"
//        );
//
//
//
//        // This pattern will validate date format YYYY-MM-dd between Feb. 20th 2014 and Dec. 31st 2399.
//        // It enforces 28/30/31 days in each month and will accept Feb 29th in leap years only.
//        Pattern PATTERN_AS_AT = Pattern.compile(
//                // 2014
//                "^(2014-02-2[0-8]|2014-((0[469]|11)-(0[1-9]|[12][0-9]|30)|(0[3578]|1[02])-(0[0-9]|[12][0-9]|3[01]))|" +
//                // 2015 - 2399
//                "((20(1[5-9]|[2-9][0-9])|2[1-3][0-9]{2})-((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01])|(0[469]|11)-(0[1-9]|[12][0-9]|30)|02-(0[1-9]|1[0-9]|2[0-8])))|" +
//                // Leap year correction
//                "(2016|20([2468][048]|[3579][26])|2[1-3]([02468][048]|[13579][26]))-02-29)$");




//        Pattern PATTERN_AS_AT = Pattern.compile(
//                        "^(2014-(02-2[0-8]|(0[3-9]|1[0-2])-(0[1-9]|1[0-9]|3[0-1]))|" +
//                        "20(1[5-9]|[2-9][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])|" +
//                        "2[1-9][0-9]{2}-(0[1-9]|1[0-2])-(0[1-9]|2[0-9]|3[0-1])|" +
//                        "[3-9][0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|2[0-9]|3[0-1]))$");

        /*
        String f = "2014-(02-2[0-8]|(0[3-9]|1[0-2])-(0[1-9]|1[0-9]|3[0-1]))";

        Pattern PATTERN_AS_AT = Pattern.compile("^20[1-9][0-9]-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$");
         */


//        String good1 = "2016-07-20";
//        String good2 = "2014-02-20";
//        String good3 = "2014-03-19";
//        String good4 = "2015-01-30";
//        String good5 = "2022-01-18";
//        String good6 = "2133-07-31";
//        String good7 = "2399-01-31";
//        String good8 = "2276-02-29";
//        String good9 = "2016-02-29";
//
//        assert PATTERN_AS_AT.matcher(good1).matches();
//        assert PATTERN_AS_AT.matcher(good2).matches();
//        assert PATTERN_AS_AT.matcher(good3).matches();
//        assert PATTERN_AS_AT.matcher(good4).matches();
//        assert PATTERN_AS_AT.matcher(good5).matches();
//        assert PATTERN_AS_AT.matcher(good6).matches();
//        assert PATTERN_AS_AT.matcher(good7).matches();
//        assert PATTERN_AS_AT.matcher(good8).matches();
//        assert PATTERN_AS_AT.matcher(good9).matches();
//
//
//        String bad1 = "16-20-07";
//        String bad2 = "2016-20-07";
//        String bad3 = "2016-07-3";
//        String bad4 = "2016-07-32";
//        String bad5 = "2014-01-21";
//        String bad6 = "2014-04-31";
//        String bad7 = "2275-02-29";
//        String bad8 = "2012-02-29";
////        String bad9 = null;
//        String bad10 = "";
//
//        assert !PATTERN_AS_AT.matcher(bad1).matches();
//        assert !PATTERN_AS_AT.matcher(bad2).matches();
//        assert !PATTERN_AS_AT.matcher(bad3).matches();
//        assert !PATTERN_AS_AT.matcher(bad4).matches();
//        assert !PATTERN_AS_AT.matcher(bad5).matches();
//        assert !PATTERN_AS_AT.matcher(bad6).matches();
//        assert !PATTERN_AS_AT.matcher(bad7).matches();
//        assert !PATTERN_AS_AT.matcher(bad8).matches();
////        assert !PATTERN_AS_AT.matcher(bad9).matches();
//        assert !PATTERN_AS_AT.matcher(bad10).matches();



//        String body = OBIEEClient.getReport(
//                "weblogic",
//                "biGard1911",
//                "/shared/Extranet/_portal/Renewal Reports",
//                "Vessels On Risk",
//                "Print",
//                true,
//                "4",
//                "eq",
//                "\"Dim Client\".\"Sf Company Id\"",
//                "1710",
//                "eq",
//                "\"Dim Coverage\".\"Gic Coverage Group Code\"",
//                "PI",
//                "eq",
//                "\"Dim Agreement Type\".\"Agreement Type Code\"",
//                "OWN",
//                "eq",
//                "\"Dim Broker\".\"Sf Company Id\"",
//                "35979");
//
//        System.out.println("===============> BEGIN HTML <==============");
//        System.out.println(body);
//        System.out.println("===============>  END HTML  <==============");
//
//        org.junit.Assert.assertTrue(true);
    }

}



